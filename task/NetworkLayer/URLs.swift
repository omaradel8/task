//
//  URLs.swift
//  FoodLand
//
//  Created by Omar on 04/01/2021.
//

import Foundation
import Alamofire

class URLs {
    static let Instance = URLs()
    private init() {}
    
    func getHeader() -> HTTPHeaders {
        let header = [
            "Accept" : "application/json" , "Content-Type": "application/json"]
        return HTTPHeaders(header)
    }
    
    
    private let url = "https://forkify-api.herokuapp.com/api/"
    
    func search() -> String{
        return url + "search"
    }
    
    func recipeDetails() -> String {
        return url + "get"
    }
}
