//
//  Request.swift
//  FoodLand
//
//  Created by Omar on 04/01/2021.
//

import Foundation
import Alamofire

class Request {
    class func request<T: Codable>(url: String, method: HTTPMethod, parameters: Parameters?,encoding: ParameterEncoding , headers: HTTPHeaders?,
                                   callBack:@escaping (Result<T, Error>) -> Void) {
        
        let jsonDecodeer = JSONDecoder()
        
        URLCache.shared.removeAllCachedResponses()
        
        AF.request(url, method: method, parameters: parameters, encoding: encoding, headers: headers).validate().responseJSON { (response) in
            
            switch response.result {
            case .success:
                if let _ = response.data {
                    guard let resultData = response.data else { fatalError() }
                    do {
                        let basicResponse = try jsonDecodeer.decode(T.self, from: resultData)
                        callBack(.success(basicResponse))
                    }
                    catch {
                        let requestError = TaskError(statusCode: 200, message: .decodingError)
                        callBack(.failure(requestError))
                    }
                }
            case .failure(let error):
                if error.responseCode ?? 800 < 500 {
                    guard let resultData = response.data else { fatalError() }
                    do {
                        let basicResponse = try jsonDecodeer.decode(ErrorModel.self, from: resultData)
                        callBack(.failure(basicResponse))
                    }
                    catch {
                        let requestError = TaskError(statusCode: 200, message: .decodingError)
                        callBack(.failure(requestError))
                    }
                }else{
                    let requestError = TaskError(statusCode: response.response?.statusCode ?? 800, message: .serverError)
                    callBack(.failure(requestError))
                }
            }
        }
    }
}
