//
//  ErrorModel.swift
//  FoodLand
//
//  Created by Omar on 04/01/2021.
//

import Foundation

struct TaskError: Error {
    var statusCode  : Int
    var message     : ErrorMessage
    
    init(statusCode: Int, message: ErrorMessage) {
        self.statusCode = statusCode
        self.message    = message
    }
}

enum ErrorMessage: String {
    case decodingError = "The Response From The Server Was In Invalid Format"
    case serverError = "There Was An Error In Server, Please Try Agin Later!"
}

// MARK: - ErrorModel
struct ErrorModel: Codable, Error {
    var error: String?
}
