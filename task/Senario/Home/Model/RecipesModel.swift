//
//  RecipesModel.swift
//  task
//
//  Created by Omar on 14/05/2021.
//

import Alamofire

struct RecipesModel: Codable {
    var count: Int?
    var recipes: [Recipe]?
    
    static func getRecipes(searchKey: String, callBack: @escaping (Result<RecipesModel, Error>) -> Void) {
        
        let parameters = ["q": searchKey]
        
        Request.request(url: URLs.Instance.search(), method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil, callBack: callBack)
    }
}
