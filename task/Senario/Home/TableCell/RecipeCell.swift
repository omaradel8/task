//
//  RecipeCell.swift
//  task
//
//  Created by Omar on 14/05/2021.
//

import UIKit
import SDWebImage

class RecipeCell: UITableViewCell {

    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!
    @IBOutlet weak var recipePublisherName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configure(_ data: Recipe?) {
        recipeImage.sd_setImage(with: URL(string: data?.imageURL ?? ""))
        recipeTitle.text = data?.title
        recipePublisherName.text = data?.publisher
    }
}
