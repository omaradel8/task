//
//  Home+SearchBarDelegate.swift
//  task
//
//  Created by Omar on 14/05/2021.
//

import UIKit

extension HomeViewController: UISearchBarDelegate {
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        availableFilteredSearchKeys = getHistory()
        dropDown.show()
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchText == "" {
            availableFilteredSearchKeys = getHistory()
            dropDown.show()
        }else{
            availableFilteredSearchKeys = availableSearchQueries.filter { $0.lowercased().contains(searchText.lowercased())}
            dropDown.show()
        }
    }
}
