//
//  Home+Api.swift
//  task
//
//  Created by Omar on 14/05/2021.
//

import UIKit
import NVActivityIndicatorView

extension HomeViewController: NVActivityIndicatorViewable {
    func getRecipes(searchKey: String) {
        self.startAnimating()
        RecipesModel.getRecipes(searchKey: searchKey){ [weak self] result in
            self?.stopAnimating()
            switch result {
            case .success(let data):
                self?.recipes = data
            case .failure(let error):
                if let error = error as? TaskError {
                    self?.presentFreshFeelsAlert(message: error.message.rawValue, grayButtonTitle: "", secondButtonTitle: NSLocalizedString("Ok", comment: ""), alertImage: UIImage(named: "90")!, needSecondButton: false, first: {}, second: {})
                }else{
                    self?.presentFreshFeelsAlert(message: (error as? ErrorModel)?.error ?? "", grayButtonTitle: "", secondButtonTitle: NSLocalizedString("Ok", comment: ""), alertImage: UIImage(named: "90")!, needSecondButton: false, first: {}, second: {})
                }
            }
        }
    }
}
