//
//  ViewController.swift
//  task
//
//  Created by Omar on 14/05/2021.
//

import UIKit
import DropDown
import CoreData

class HomeViewController: UIViewController {

    @IBOutlet weak var recipesTableView: UITableView!{
        didSet{
            recipesTableView.delegate = self
            recipesTableView.dataSource = self
        }
    }
    let searchController = UISearchController(searchResultsController: nil)
    let dropDown = DropDown()
    
    var availableFilteredSearchKeys: [String]?{
        didSet{
            dropDown.reloadAllComponents()
            dropDown.dataSource = availableFilteredSearchKeys ?? []
        }
    }
    var recipes: RecipesModel?{
        didSet{
            recipesTableView.reloadData()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupSearchBar()
        setupDropDown()
        self.navigationItem.title = "Find Recipes"
    }
    
    private func setupSearchBar() {
        navigationItem.searchController = searchController
        searchController.searchBar.delegate = self
    }
    
    private func setupDropDown() {
        availableFilteredSearchKeys = getHistory()
        dropDown.anchorView = searchController.searchBar
        dropDown.direction = .bottom
        dropDown.dataSource = availableFilteredSearchKeys ?? []
        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
        
        dropDown.selectionAction = { [unowned self] (index: Int, item: String) in
            self.searchController.searchBar.text = availableFilteredSearchKeys?[index]
            self.getRecipes(searchKey: availableFilteredSearchKeys?[index] ?? "")
            CoreDataManager.instance.saveNewItem(title: availableFilteredSearchKeys?[index] ?? "")
            self.searchController.isActive = false
        }
    }
    
    func getHistory() -> [String] {
        var history = [String]()
        let dataHistory = CoreDataManager.instance.fetchHistory()
        for e in dataHistory {
            history.append(e.title ?? "")
        }
        return history
    }
}
