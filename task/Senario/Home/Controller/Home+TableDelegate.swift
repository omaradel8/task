//
//  Home+TableDelegate.swift
//  task
//
//  Created by Omar on 14/05/2021.
//

import UIKit

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return recipes?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecipeCell", for: indexPath) as! RecipeCell
        cell.configure(recipes?.recipes?[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = AppStoryboard.RecipeDetails.viewController(viewControllerClass: RecipeDetailsViewController.self)
        vc.recipeId = recipes?.recipes?[indexPath.row].recipeID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
