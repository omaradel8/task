//
//  RecipeDetailsModel.swift
//  task
//
//  Created by Omar on 15/05/2021.
//

import Alamofire

struct RecipeDetailsModel: Codable {
    var recipe: Recipe?
    
    static func getRecipeDetails(rid: String, callBack: @escaping (Result<RecipeDetailsModel, Error>) -> Void) {
        
        let parameters = ["rId": rid]
        
        Request.request(url: URLs.Instance.recipeDetails(), method: .get, parameters: parameters, encoding: URLEncoding.default, headers: nil, callBack: callBack)
    }
}

// MARK: - Recipe
struct Recipe: Codable {
    var publisher: String?
    var ingredients: [String]?
    var sourceURL: String?
    var recipeID: String?
    var imageURL: String?
    var title: String?

    enum CodingKeys: String, CodingKey {
        case publisher, ingredients
        case sourceURL = "source_url"
        case recipeID = "recipe_id"
        case imageURL = "image_url"
        case title
    }
}
