//
//  HeaderDetailsView.swift
//  task
//
//  Created by Omar on 15/05/2021.
//

import UIKit

class HeaderDetailsView: UIViewController {

    @IBOutlet weak var recipeImage: UIImageView!
    @IBOutlet weak var recipeTitle: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
