//
//  RecipeDetails+Api.swift
//  task
//
//  Created by Omar on 15/05/2021.
//

import UIKit
import NVActivityIndicatorView

extension RecipeDetailsViewController: NVActivityIndicatorViewable {
    func getRecipeDetails() {
        self.startAnimating()
        RecipeDetailsModel.getRecipeDetails(rid: recipeId ?? "") { [weak self] result in
            self?.stopAnimating()
            switch result {
            case .success(let data):
                self?.recipeDetails = data
            case .failure(let error):
                if let error = error as? TaskError {
                    self?.presentFreshFeelsAlert(message: error.message.rawValue, grayButtonTitle: "", secondButtonTitle: NSLocalizedString("Ok", comment: ""), alertImage: UIImage(named: "90")!, needSecondButton: false, first: {}, second: {})
                }else{
                    self?.presentFreshFeelsAlert(message: (error as? ErrorModel)?.error ?? "", grayButtonTitle: "", secondButtonTitle: NSLocalizedString("Ok", comment: ""), alertImage: UIImage(named: "90")!, needSecondButton: false, first: {}, second: {})
                }
            }
        }
    }
}
