//
//  RecipeDetailsViewController.swift
//  task
//
//  Created by Omar on 15/05/2021.
//

import UIKit

class RecipeDetailsViewController: UIViewController {

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var recipeDetailsTableView: UITableView!{
        didSet{
            recipeDetailsTableView.isHidden = true
            recipeDetailsTableView.delegate = self
            recipeDetailsTableView.dataSource = self
        }
    }
    
    var recipeId: String?
    var recipeDetails: RecipeDetailsModel?{
        didSet{
            recipeDetailsTableView.isHidden = false
            recipeDetailsTableView.reloadData()
            configureHeader()
        }
    }
    var vc = AppStoryboard.RecipeDetails.viewController(viewControllerClass: HeaderDetailsView.self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getRecipeDetails()
        setupHeader()
    }
    
    private func setupHeader() {
        addChild(vc)
        headerView.addSubview(vc.view)
        vc.didMove(toParent: self)
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        vc.view.bottomAnchor.constraint(equalToSystemSpacingBelow: headerView.bottomAnchor, multiplier: 0).isActive = true
        vc.view.topAnchor.constraint(equalToSystemSpacingBelow: headerView.topAnchor, multiplier: 0).isActive = true
        vc.view.leadingAnchor.constraint(equalToSystemSpacingAfter: headerView.leadingAnchor, multiplier: 0).isActive = true
        vc.view.trailingAnchor.constraint(equalToSystemSpacingAfter: headerView.trailingAnchor, multiplier: 0).isActive = true
    }

    private func configureHeader() {
        vc.recipeImage.sd_setImage(with: URL(string: recipeDetails?.recipe?.imageURL ?? ""))
        vc.recipeTitle.text = recipeDetails?.recipe?.title
    }
}
