//
//  CoreDataManager.swift
//  task
//
//  Created by Omar on 15/05/2021.
//

import CoreData

final class CoreDataManager {
    
    static let instance = CoreDataManager()
    private init(){}
    
    lazy var persistentContainer: NSPersistentContainer = {
        let persistentContainer = NSPersistentContainer(name: "History")
        persistentContainer.loadPersistentStores { (_, error) in
            print(error?.localizedDescription ?? "")
        }
        return persistentContainer
    }()
    
    var moc: NSManagedObjectContext {
        persistentContainer.viewContext
    }
    
    var maxCapacity = 10
    
    func saveNewItem(title: String) {
        let newHistory = HistoryEntity(context: moc)
        newHistory.setValue(title, forKey: "title")
        
        do {
            try  moc.save()
            checkOnMaxCapacity()
        }catch {
            print(error)
        }
    }
    
    func fetchHistory() -> [HistoryEntity] {
        do {
            let fetchRequest = NSFetchRequest<HistoryEntity>(entityName: "HistoryEntity")
            let history = try moc.fetch(fetchRequest)
            return history.reversed()
        }catch {
            print(error)
            return []
        }
    }
    
    func checkOnMaxCapacity() {
        let data = fetchHistory()
        if data.count > maxCapacity {
            deleteLastHistory(object: data.last!)
        }
    }
    
    func deleteLastHistory(object: HistoryEntity){
        moc.delete(object)
        
        do {
            try moc.save()
        }catch {
            print(error)
        }
    }
}
